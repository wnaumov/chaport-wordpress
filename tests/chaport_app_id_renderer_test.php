<?php
use PHPUnit\Framework\TestCase;

require_once(dirname(__FILE__) . '/../includes/models/chaport_app_id.php');
require_once(dirname(__FILE__) . '/../includes/renderers/chaport_app_id_renderer.php');

final class ChaportAppIdRendererTest extends TestCase {
    private const APP_ID = '1234567890abcdef12345678';

    public function testRendersCodeSnippet() {
        $renderer = new ChaportAppIdRenderer(ChaportAppId::fromString(self::APP_ID));
        $snippet = $renderer->renderToString();
        $this->assertContains(self::APP_ID, $snippet);
        $this->assertContains('Chaport Live Chat code', $snippet);
        $this->assertContains('script type="text/javascript"', $snippet);
    }

    public function testRendersNameAndEmailWhenAvailable() {
        $renderer = new ChaportAppIdRenderer(ChaportAppId::fromString(self::APP_ID));
        $snippet = $renderer->renderToString();
        $this->assertNotContains('visitorData.email =', $snippet);
        $this->assertNotContains('visitorData.name =', $snippet);
        $renderer->setUserEmail('john.doe@example.com');
        $renderer->setUserName('John Doe');
        $snippet = $renderer->renderToString();
        $this->assertContains('visitorData.email = \'john.doe@example.com\'', $snippet);
        $this->assertContains('visitorData.name = \'John Doe\'', $snippet);
    }
}
